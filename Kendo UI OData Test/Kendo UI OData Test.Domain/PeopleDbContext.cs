﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kendo_UI_OData_Test.Domain
{
    public class PeopleDbContext : DbContext
    {
        public PeopleDbContext()
            : base("name=people")
        {
        }

        public System.Data.Entity.DbSet<Kendo_UI_OData_Test.Domain.Entities.Person> People { get; set; }
    }
}
