﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;


using System.Web.Http.OData.Builder;
using System.Web.Http.OData.Extensions;
using Kendo_UI_OData_Test.Domain.Entities;

namespace Kendo_UI_OData_Test
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


            ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<Person>("People");
            config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
        }
    }
}
